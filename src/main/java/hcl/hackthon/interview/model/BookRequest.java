package hcl.hackthon.interview.model;


public class BookRequest {

	private String title;
	private String author;
	private Long isbnNo;
	private String category;
	private String qty;
	private Integer rackNo;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Long getIsbnNo() {
		return isbnNo;
	}

	public void setIsbnNo(Long isbnNo) {
		this.isbnNo = isbnNo;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public Integer getRackNo() {
		return rackNo;
	}

	public void setRackNo(Integer rackNo) {
		this.rackNo = rackNo;
	}

}
