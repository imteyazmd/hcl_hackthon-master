package hcl.hackthon.interview.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BookRequestWrapper {

	@JsonProperty(value="BookRequest")
	private BookRequest bookRequest;

	public BookRequest getBookRequest() {
		return bookRequest;
	}

	public void setBookRequest(BookRequest bookRequest) {
		this.bookRequest = bookRequest;
	}
	
}
