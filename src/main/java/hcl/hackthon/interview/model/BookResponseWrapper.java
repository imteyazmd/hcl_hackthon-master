package hcl.hackthon.interview.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BookResponseWrapper {

	@JsonProperty(value = "ResponseHeader")
	private ResponseHeader responseHeader;

	@JsonProperty(value = "BookResponse")
	private List<BookResponse> response;

	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	public void setResponseHeader(ResponseHeader responseHeader) {
		this.responseHeader = responseHeader;
	}

	public List<BookResponse> getResponse() {
		return response;
	}

	public void setResponse(List<BookResponse> response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "BookResponseWrapper [responseHeader=" + responseHeader
				+ ", response=" + response + "]";
	}
	
	
}
