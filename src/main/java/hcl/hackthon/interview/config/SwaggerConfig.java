package hcl.hackthon.interview.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {
	@Bean
	public Docket beneficiarySwaggerApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.forCodeGeneration(true)
				.apiInfo(apiInfo())
				.useDefaultResponseMessages(false)
				.select()
				.apis(RequestHandlerSelectors
						.basePackage("hcl.hackthon.interview.rest"))
				.paths(PathSelectors.any()).build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("HCL Interview")
				.description("HCL Interview")
				.contact(
						new Contact("Md Imteyaz", "8521954181",
								"imteyazmd29@gmail.com"))
				.license("Proprietary").licenseUrl("").termsOfServiceUrl("")
				.version("1.0.0").build();
	}
}