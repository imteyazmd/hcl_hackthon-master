package hcl.hackthon.interview.service;

import hcl.hackthon.interview.model.BookRequest;
import hcl.hackthon.interview.model.BookResponseWrapper;

public interface BookService {

	BookResponseWrapper addBook(BookRequest request);

	BookResponseWrapper searchBook(BookRequest request);

}
