package hcl.hackthon.interview.service;

import hcl.hackthon.interview.entity.Book;
import hcl.hackthon.interview.model.BookRequest;
import hcl.hackthon.interview.model.BookResponse;
import hcl.hackthon.interview.model.BookResponseWrapper;
import hcl.hackthon.interview.model.ResponseHeader;
import hcl.hackthon.interview.repo.BookRepo;
import hcl.hackthon.interview.util.CommonUtility;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookRepo bookRepo;

	private static final Logger logger = LoggerFactory
			.getLogger(BookServiceImpl.class);

	public BookResponseWrapper addBook(BookRequest request) {
		logger.info("addBook");
		Book book = new Book();
		ResponseHeader header = new ResponseHeader();
		BeanUtils.copyProperties(request, book);
		BookResponseWrapper responseWrapper = new BookResponseWrapper();
		List<BookResponse> bookResponseList = new ArrayList<BookResponse>();
		BookResponse bookResponse = new BookResponse();
		try {
			Book savedBook = bookRepo.save(book);
			header.setRespCode(CommonUtility.SUCCESS_RESPCODE);
			BeanUtils.copyProperties(savedBook, bookResponse);
			bookResponseList.add(bookResponse);
			header.setHttpStatus(HttpStatus.OK);
		} catch (Exception e) {
			header.setRespCode(CommonUtility.FAILURE_RESPCODE);
			header.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		responseWrapper.setResponse(bookResponseList);
		responseWrapper.setResponseHeader(header);
		return responseWrapper;
	}

	public BookResponseWrapper searchBook(BookRequest request) {
		// title category
		logger.info("searchBook");

		ResponseHeader header = new ResponseHeader();
		BookResponseWrapper responseWrapper = new BookResponseWrapper();
		BookResponse bookResponse = null;
		List<BookResponse> bookResponseList = new ArrayList<BookResponse>();
		List<Book> bookListResponse = new ArrayList<Book>();

		try {
			bookListResponse = bookRepo.findByTitleOrCategoryOrAuthor(
					request.getTitle(), request.getCategory(),
					request.getAuthor());
			header.setRespCode(CommonUtility.SUCCESS_RESPCODE);
			header.setHttpStatus(HttpStatus.OK);
		} catch (Exception e) {
			header.setRespCode(CommonUtility.FAILURE_RESPCODE);
			header.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		logger.info("bookListResponse" + bookListResponse.size());

		for (Book book : bookListResponse) {
			bookResponse = new BookResponse();
			BeanUtils.copyProperties(book, bookResponse);
			bookResponseList.add(bookResponse);
		}

		responseWrapper.setResponse(bookResponseList);
		responseWrapper.setResponseHeader(header);
		return responseWrapper;
	}

}
