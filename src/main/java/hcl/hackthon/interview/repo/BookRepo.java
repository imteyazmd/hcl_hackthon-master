package hcl.hackthon.interview.repo;

import hcl.hackthon.interview.entity.Book;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepo extends CrudRepository<Book, Long> {
	List<Book> findByTitleOrCategoryOrAuthor(String title, String category,
			String author);
}
