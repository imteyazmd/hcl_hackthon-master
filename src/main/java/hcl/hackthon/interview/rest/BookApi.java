package hcl.hackthon.interview.rest;

import hcl.hackthon.interview.model.BookRequest;
import hcl.hackthon.interview.model.BookResponseWrapper;
import hcl.hackthon.interview.service.BookService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
public class BookApi {

	@Autowired
	private BookService bookService;

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BookResponseWrapper> addBook(@RequestBody BookRequest book) {
		BookResponseWrapper response = bookService.addBook(book);
		return new ResponseEntity<BookResponseWrapper>(response, response.getResponseHeader().getHttpStatus());
		
	}

	@PostMapping(value = "/search", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BookResponseWrapper>  searchBook(
			@RequestBody BookRequest book) {
		BookResponseWrapper response = bookService.searchBook(book);
		return new ResponseEntity<BookResponseWrapper>(response, response.getResponseHeader().getHttpStatus());
	}
}
